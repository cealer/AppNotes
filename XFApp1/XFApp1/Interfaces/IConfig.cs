﻿using SQLite.Net.Interop;

namespace XFApp1.Interfaces
{
    public interface IConfig
    {
        string DirectoryDB { get; }

        ISQLitePlatform Platform { get; }

    }
}