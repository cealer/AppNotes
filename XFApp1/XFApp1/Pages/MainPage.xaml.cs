﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XFApp1.Clases;

namespace XFApp1.Pages
{
    public partial class MainPage : ContentPage
    {
        //Para que prevalesca en toda la ejecucion
        private Users user;

        public MainPage(Users user)
        {
            InitializeComponent();
            this.Padding = Device.OnPlatform(new Thickness(10, 20, 10, 10), new Thickness(10), new Thickness(10));
            this.user = user;

            logOutButton.Clicked += LogOutButton_Clicked;

        }

        private async void LogOutButton_Clicked(object sender, EventArgs e)
        {
            using (var db = new DataAccess())
            {
                var user = db.First<Users>();
                if (user != null)
                {
                    db.Delete(user);
                }
                await Navigation.PushAsync(new LoginPage());
            }
        }

        //Cuando haya cargado el constructor se ejecta
        protected override void OnAppearing()
        {
            base.OnAppearing();
            userNameLabel.Text = this.user.FullName;
            photoImage.Source = this.user.PhotoFullPath;
            photoImage.WidthRequest = 280;
            photoImage.HeightRequest = 280;

            if (this.user.IsTeacher == true)
            {
                myNotesButton.IsVisible = false;
            }
            else
            {
                enterNotesButton.IsVisible = false;
            }
        }

    }
}
