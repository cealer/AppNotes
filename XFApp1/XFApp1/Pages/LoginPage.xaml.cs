﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XFApp1.Clases;

namespace XFApp1.Pages
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            this.Padding = Device.OnPlatform(new Thickness(10, 20, 10, 10), new Thickness(10), new Thickness(10));

            enterButton.Clicked += EnterButton_Clicked;

        }

        protected override void OnAppearing()
        {
            waitActivityIndicator.IsRunning = true;
            waitActivityIndicator.IsRunning = false;
        }

        private async void EnterButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(emailEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar una dirección de correo.", "Aceptar");
                emailEntry.Focus();
                return;
            }

            if (!Utilities.IsValidEmail(emailEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar una dirección de correo válida.", "Aceptar");
                emailEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(passwordEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar una contraseña.", "Aceptar");
                passwordEntry.Focus();
                return;
            }

            this.Login();

        }

        private async void Login()
        {
            waitActivityIndicator.IsRunning = true;

            var loginRequest = new LoginRequest
            {
                Email = emailEntry.Text,
                Password = passwordEntry.Text
            };

            var jsonRequest = JsonConvert.SerializeObject(loginRequest);
            var httpContent = new StringContent(jsonRequest, System.Text.Encoding.UTF8, "application/json");
            var response = string.Empty;

            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(@"http://del4to.com");
                var url = "/API/Users/Login";
                var result = await client.PostAsync(url, httpContent);


                if (!result.IsSuccessStatusCode)
                {
                    await DisplayAlert("Error", "Usuario o contraseña incorrectos.", "Aceptar");
                    waitActivityIndicator.IsRunning = false;
                    return;
                }
                response = await result.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                waitActivityIndicator.IsRunning = false;
                await DisplayAlert("Error", ex.Message, "Aceptar");
                return;
            }

            var user = JsonConvert.DeserializeObject<Users>(response);
            user.Password = passwordEntry.Text;
            this.VerifyRememberMe(user);
            waitActivityIndicator.IsRunning = false;
            //Navigation es una pila por la tanto tenemos push o pop
            await Navigation.PushAsync(new MainPage(user));


        }

        private void VerifyRememberMe(Users user)
        {
            if (remebermeSwitch.IsToggled)
            {
                using (var db = new DataAccess())
                {
                    db.Insert<Users>(user);
                }
            }
        }
    }
}