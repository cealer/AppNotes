using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using XFApp1.Clases;
using XFApp1.Pages;

namespace XFApp1
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            using (var db = new DataAccess())
            {
                var user = db.First<Users>();
                if (user == null)
                {

                    this.MainPage = new NavigationPage(new LoginPage());
                }
                else
                {
                    this.MainPage = new NavigationPage(new MainPage(user));
                }
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
